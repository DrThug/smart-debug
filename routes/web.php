<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/bug', 'BugController@index')->name('bug');
Route::get('/blog', 'BlogController@index')->name('blog');
Route::get('/bug/{id}', 'BugController@detail')->name('bugDetail');
Route::get('/blog/{id}', 'BlogController@detail')->name('articleDetail');

Auth::routes();

Route::get('/admin','Dashboard@index')->name('admin');
Route::group(['prefix' => 'admin'], function () {
    /* Routes pour les opérations sur les utilisateurs */
    Route::get('/users','UserController@index')->name('users');
    Route::get('/users/edit/{id}','UserController@edit')->name('editUser');
    Route::get('/users/add','UserController@add')->name('users/add');
    Route::post('/users/store', 'UserController@store')->name('storeUser');
    Route::post('/users/update', 'UserController@update')->name('updateUser');
    
    /* Routes pour les opérations sur les articles et bugs */
    Route::get('/articles','ArticleController@index')->name('articles');
    Route::get('/articles/edit/{id}','ArticleController@edit')->name('editArticles');
    Route::get('/articles/add','ArticleController@add')->name('articles/add');
    Route::post('/articles/store', 'ArticleController@store')->name('storeArticles');
    Route::post('/articles/update', 'ArticleController@update')->name('updateArticles');
});

