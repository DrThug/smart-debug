@extends('Layouts.header')

@section('contentFront')

<!-- Features -->
    <div id="features" class="tabs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>DERNIERS BUGS & ARTICLES</h2>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="container-fluid">
                
                @foreach ($bugs as $bug)
                <!-- Details 1 -->
                <div id="details" class="basic-2" style="background-color:#2f2c3d;">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <img class="img-fluid" src="{{asset('images/video-frame.jpg')}}" alt="alternative">
                            </div> <!-- end of col -->
                            <div class="col-lg-6">
                                <div class="text-container">
                                    <h3>{{ $bug->titre }}</h3>
                                    <h5>{{ $bug->intitule }}</h5><br>
                                    <a class="btn-solid-reg popup-with-move-anim" href="{{url('/bug/')}}/{{$bug->id}}">VOIR PLUS +</a>
                                </div> <!-- end of text-container -->
                            </div> <!-- end of col -->
                        </div> <!-- end of row -->
                    </div> <!-- end of container -->
                </div> <!-- end of basic-2 -->
                <!-- end of details 1 -->
                @endforeach

                @foreach ($articles as $bug)
                <!-- Details 2 -->
                <div class="basic-3" style="background-color:#2f2c3d;">
                    <div class="second">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="text-container">
                                        <h3>{{ $bug->titre }}</h3>
                                        <h5>{{ $bug->intitule }}</h5><br>
                                        <a class="btn-solid-reg popup-with-move-anim" href="{{url('/blog/')}}/{{$bug->id}}">VOIR PLUS +</a>
                                    </div> <!-- end of text-container -->
                                </div> <!-- end of col -->
                                <div class="col-lg-6">
                                    <img class="img-fluid" src="{{asset('images/video-frame.jpg')}}" alt="alternative">
                                </div> <!-- end of col -->
                            </div> <!-- end of row -->
                        </div> <!-- end of container -->
                    </div> <!-- end of second -->
                </div> <!-- end of basic-3 -->  
                @endforeach  
                <!-- end of details 2 -->

                
            </div> <!-- end of row --> 
        </div> <!-- end of container --> 
    </div> <!-- end of tabs -->
    <!-- end of features -->

    <!-- Testimonials -->
    <div class="slider-1">
    <h2 style="
    text-align: center;
    padding-bottom: 5rem;">LA TEAM</h2>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <!-- Card Slider -->
                    <div class="slider-container">
                        <div class="swiper-container card-slider">
                            <div class="swiper-wrapper">
                                
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('img/team/Manathan.jpg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">Dr Manhattan - Re !</p>
                                            <p class="testimonial-author">Rémi</p>
                                        </div>
                                    </div>
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->

                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('img/team/Yvon.jpg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">Dr Strange King Lion - On en est où ?</p>
                                            <p class="testimonial-author">Yvon FANFE</p>
                                        </div>
                                    </div>
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
                                
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('img/team/Mannonni.jpg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">V - Bonjour !</p>
                                            <p class="testimonial-author">Alexandre MANNONNI</p>
                                        </div>
                                    </div>
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
                                
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('img/team/Boris.jpg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">Pr Charles Xavier - Mbrikiti !</p>
                                            <p class="testimonial-author">Boris MOUCHELI - Developpeur web/mobile</p>
                                        </div>
                                    </div>
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
                                
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('img/team/Aristide.jpg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">Batman - Mbrakata !</p>
                                            <p class="testimonial-author">Aristide BESSALA - Developpeur web</p>
                                        </div>
                                    </div>
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
        
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('img/team/Thug.jpg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">Black Panther - Zi Extension</p>
                                            <p class="testimonial-author">Stephane TOUKAM - Developpeur web/mobile</p>
                                        </div>
                                    </div>        
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
        
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('img/team/Alladin.jpg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">Spiderman - Pas mal là-bas !</p>
                                            <p class="testimonial-author">Alladin Avaika - Developpeur web</p>
                                        </div>
                                    </div>        
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
        
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('img/team/Claude.jpg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">Captain America - Les normes !</p>
                                            <p class="testimonial-author">Claude DJO - Developpeur web</p>
                                        </div>
                                    </div>        
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
        
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('img/team/Galactus.jpg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">Galactus - !</p>
                                            <p class="testimonial-author">Ernest - Designer</p>
                                        </div>
                                    </div>        
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
        
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('img/team/Teddy.jpg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">Thor - !</p>
                                            <p class="testimonial-author">Teddy KAMGANG - Developpeur web</p>
                                        </div>
                                    </div>        
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
        
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('img/team/Parfait.jpg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">Flash - Pardon monsieur ?</p>
                                            <p class="testimonial-author">Parfait Ebobisse - Developpeur web/mobile</p>
                                        </div>
                                    </div>        
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
        
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('img/team/Hadi.png')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">Wonder-Woman - Ca ne donne pas !</p>
                                            <p class="testimonial-author">Hadizatou SOUNOUSSI</p>
                                        </div>
                                    </div>        
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
        
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('img/team/Lucry.jpg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">RAZORBACK - !</p>
                                            <p class="testimonial-author">Lucry - Developpeur web</p>
                                        </div>
                                    </div>        
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
        
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('img/team/Vincent.jpg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">Wolverine - !</p>
                                            <p class="testimonial-author">Vincent - Developpeur web</p>
                                        </div>
                                    </div>        
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
        
                            
                            </div> <!-- end of swiper-wrapper -->
        
                            <!-- Add Arrows -->
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                            <!-- end of add arrows -->
        
                        </div> <!-- end of swiper-container -->
                    </div> <!-- end of slider-container -->
                    <!-- end of card slider -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of slider-1 -->
    <!-- end of testimonials -->

    <!-- Contact -->
    <div id="contact" class="form">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>CONTACT</h2>
                    <ul class="list-unstyled li-space-lg">
                        <!-- <li class="address">Don't hesitate to give us a call or just use the contact form below</li> -->
                        <li><i class="fas fa-map-marker-alt"></i>Douala, Cameroun</li>
                        <li><i class="fas fa-phone"></i><a class="blue" href="tel:003024630820">+237 699999999</a></li>
                        <li><i class="fas fa-envelope"></i><a class="blue" href="mailto:office@leno.com">contact@smartcode.com</a></li>
                    </ul>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    
                    <!-- Contact Form -->
                    <form id="contactForm" data-toggle="validator" data-focus="false">
                        <div class="form-group">
                            <input type="text" class="form-control-input" id="cname" required>
                            <label class="label-control" for="cname">Name</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control-input" id="cemail" required>
                            <label class="label-control" for="cemail">Email</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control-textarea" id="cmessage" required></textarea>
                            <label class="label-control" for="cmessage">Your message</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group checkbox">
                            <input type="checkbox" id="cterms" value="Agreed-to-Terms" required>I have read and agree to Leno's stated conditions in <a href="privacy-policy.html">Privacy Policy</a> and <a href="terms-conditions.html">Terms Conditions</a> 
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="form-control-submit-button">SUBMIT MESSAGE</button>
                        </div>
                        <div class="form-message">
                            <div id="cmsgSubmit" class="h3 text-center hidden"></div>
                        </div>
                    </form>
                    <!-- end of contact form -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of form -->
    <!-- end of contact -->
@endsection