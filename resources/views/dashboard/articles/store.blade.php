@extends('Dashboard.Layout.header')
@extends('Dashboard.Layout.sidebar')

@section('content')

<style>
    .loading .fa{
        display:none !important;
    }
    .loading .add{
        display:none !important;
    }
    .loading .loader{
        display:block !important;
    }
    .element{
        display: none;
    }
</style>
<div class="content" style="padding-top:30px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="card ">
                    <div class="card-header ">
                        <h4 class="card-title"></h4>
                    </div>
                    <div class="card-body ">
                        <form method="POST" action="{{ route('storeArticles') }}" class="form-horizontal" id="storeArticle">
                        @csrf
                            <div class="row" style="padding-left:20px;padding-right:20px;padding-bottom:30px;">
                                <select name="isBug" id="isBug" class="form-control">
                                    <option value="">--- Que voulez-vous créer ? ---</option>
                                    <option value="article">Article</option>
                                    <option value="bugs">Bugs</option>
                                </select>
                            </div>
                            <div class="article row element">
                                <div class="col-md-12">
                                    <label class="control-label">Titre *</label>
                                    <div>
                                        <!-- has-success -->
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="titreArticle" id="titreArticle" placeholder="Titre de l'article">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="control-label">Contenu *</label>
                                    <div>
                                        <!-- has-success -->
                                        <div class="form-group">
                                            <textarea id="summernoteArticle" name="contenu"></textarea> 
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="control-label">Catégorie *</label>
                                    <div>
                                        <!-- has-success -->
                                        <div class="form-group">
                                            <select name="role" id="role" class="form-control" required>
                                                @foreach($categories as $role)

                                                    <option value="{{ $role->id }}">{{ $role->intitule }}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bug row element">
                                <div class="col-md-12">
                                    <label class="control-label">Titre *</label>
                                    <div>
                                        <!-- has-success -->
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="titreBug" id="titreBug" placeholder="Titre du bug">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="control-label">Solution *</label>
                                    <div>
                                        <!-- has-success -->
                                        <div class="form-group">
                                            <textarea id="summernote" name="solution" id="solution"></textarea> 
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="control-label">Catégorie *</label>
                                    <div>
                                        <!-- has-success -->
                                        <div class="form-group">
                                            <select name="categorie" id="categorie" class="form-control" required>
                                                @foreach($categories as $role)

                                                    <option value="{{ $role->id }}">{{ $role->intitule }}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-wd btn-success" id="submitBtn" style="margin-top: 2.5rem;">
                                        <span class="btn-label">
                                            <i class="fa fa-plus"></i>
                                        </span>
                                        <img src="{{asset('img/loading.gif')}}" class="loader" style="height: auto;
                                        width: 1.2rem;display:none;margin-left: 0.2rem;">
                                        <span class="add">Créer</span>
                                    </button>
                                </div>
                            </div>
                            
                            
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $('#summernote').summernote({
      placeholder: 'Hello stand alone ui',
      tabsize: 2,
      height: 120,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
      ]
    });

    $('#summernoteArticle').summernote({
      placeholder: 'Hello stand alone ui',
      tabsize: 2,
      height: 200,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
      ]
    });
  </script>
<script>
    $("#isBug").on('change', function () {
        var value = this.value;
        console.log(value);
        if (value == "article") {
            $(".element").hide();
            $(".article").show(500);
        }else if (value == "bugs"){
            $(".element").hide();
            $(".bug").show(500);
        }else{
            $(".element").hide();
        }
    })
</script>

<script>
    $('#storeArticle').on('submit', function (e) {

                            e.preventDefault();
                            $('#submitBtn').addClass("loading");
                            var isBug = $("#isBug").val();
                            if (isBug == "article") {
                                $(".bugs").remove();
                            } else if(isBug == "bugs") {
                                $(".article").remove();
                            }
                            var idUser = {{ Auth::user()->id }};
                            var $form = $(this);

                            var formdata = (window.FormData) ? new FormData($form[0]) : null;

                            var data = (formdata !== null) ? formdata : $form.serialize();
                            data.append("idUser",idUser);
                            data.append("element",isBug);
                            $.ajax({
                                    url: $form.attr('action'),
                                    type: $form.attr('method'),
                                    contentType: false,
                                    processData: false,
                                    data: data,
                                    success: function (reponse) {
                                        swal(
                                                'Bravo !',
                                                'Article/Bug crée avec succès.',
                                                'success'
                                            );
                                            $('#submitBtn').removeClass("loading");
                                            window.location.href = "{{url('admin/articles')}}";
                                    },
                                    error: function (reponse) {
                                        swal(
                                            'Erreur !',
                                            'Une erreur est survenue. Veuillez reesayer plutard.',
                                            'error'
                                        );
                                        $('#submitBtn').removeClass("loading");
                                        console.log('responserror' + reponse);
                                    }

                                });
                            });
</script>

@endpush



