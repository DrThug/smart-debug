@extends('Dashboard.Layout.header')
@extends('Dashboard.Layout.sidebar')

@section('content')
<div class="content" style="padding-top:30px;">
    <div class="container-fluid">
      <div style="display:flex;flex-direction:row;">
        <a href="{{url('admin/articles/add')}}" class="btn btn-wd btn-success btn-outline">
            <span class="btn-label">
                <i class="fa fa-plus"></i>
            </span>
            Ajouter un article/bug
        </a>
        <select name="isBug" id="isBug" class="form-control" style="width: 20.5%;">
          <option value="">--- Voir Bugs ---</option>
          <option value="article">Articles</option>
          <option value="bugs">Bugs</option>
        </select>
        </div>
        <div class="row" style="padding-top:20px;">
            <div class="col-md-12">
                <div class="row">
        <div class="col article">
          <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
              <h3 class="text-white mb-0">Liste des articles</h3>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-dark table-flush">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col" class="sort" data-sort="name">Titre</th>
                    <th scope="col" class="sort" data-sort="budget">Categorie</th>
                    <th scope="col" class="sort" data-sort="status">Crée le</th>
                    <th scope="col" class="sort" data-sort="status">Crée par</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody class="list">
                  @foreach ($articles as $user)
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                        <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="{{asset('favicon.ico')}}">
                        </a>
                        <div class="media-body">
                          <span class="name mb-0 text-sm">{{ $user->titre }}</span>
                        </div>
                      </div>
                    </th>
                    <td>{{ $user->intitule }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td>{{ $user->name }}</td>
                    <td class="text-right">
                        <a href="{{url('admin/articles/edit')}}/{{$user->id}}"><i class="fa fa-edit"></i></a>
                        <a href="{{url('/blog/')}}/{{$user->id}}"><i class="fa fa-eye"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col bugs" style="display:none;">
          <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
              <h3 class="text-white mb-0">Liste des bugs</h3>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-dark table-flush">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col" class="sort" data-sort="name">Titre</th>
                    <th scope="col" class="sort" data-sort="budget">Categorie</th>
                    <th scope="col" class="sort" data-sort="status">Crée le</th>
                    <th scope="col" class="sort" data-sort="status">Crée par</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody class="list">
                  
                  @foreach ($bugs as $user)
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                        <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="{{asset('favicon.ico')}}">
                        </a>
                        <div class="media-body">
                          <span class="name mb-0 text-sm">{{ $user->titre }}</span>
                        </div>
                      </div>
                    </th>
                    <td>{{ $user->intitule }}</td>
                    <td>{{ $user->created_at }}</td>
                    <td>{{ $user->name }}</td>
                    <td class="text-right">
                        <a href="{{url('admin/articles/edit')}}/{{$user->id}}"><i class="fa fa-edit"></i></a>
                        <a href="{{url('/bug/')}}/{{$user->id}}"><i class="fa fa-eye"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')

<script>
  $("#isBug").on('change', function () {
      var value = this.value;
      console.log(value);
      if (value == "article") {
          $(".bugs").hide(200);
          $(".article").show(500);
      }else if (value == "bugs"){
          $(".article").hide(200);
          $(".bugs").show(500);
      }else{
          $(".bugs").hide(200);
          $(".article").show(500);
      }
  })
</script>
<script type="text/javascript">
    
</script>
@endpush



