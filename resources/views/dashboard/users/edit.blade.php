@extends('Dashboard.Layout.header')
@extends('Dashboard.Layout.sidebar')

@section('content')

<style>
    .loading .fa{
        display:none !important;
    }
    .loading .fa-pencil{
        display:none !important;
    }
    .loading .add{
        display:none !important;
    }
    .loading .loader{
        display:block !important;
    }
</style>
<div class="content" style="padding-top:30px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="card ">
                    <div class="card-header ">
                        <h4 class="card-title"></h4>
                    </div>
                    <div class="card-body ">
                        <form method="POST" action="{{ route('updateUser') }}" class="form-horizontal" id="updateUser">
                        @csrf

                            <input type="hidden" name="id" value="{{ $utilisateur->id }}">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Nom *</label>
                                    <div>
                                        <!-- has-success -->
                                        <div class="form-group">
                                        <input type="text" name="name" id="name" class="form-control" required value="{{ $utilisateur->name }}"  placeholder="Entrez le nom">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Email *</label>
                                    <div>
                                        <!-- has-error -->
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" class="form-control" value="{{ $utilisateur->email }}" required placeholder="Entrez l'email">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Rôle *</label>
                                    <div>
                                        <!-- has-success -->
                                        <div class="form-group">
                                            <select name="role" id="role" class="form-control" required>
                                                @foreach($roles as $role)
                                                    @if($role->role == $utilisateur->role)
                                                        <option value="{{ $role->role }}" selected>{{ $role->role }}</option>
                                                    @else
                                                        <option value="{{ $role->role }}">{{ $role->role }}</option>
                                                    @endif

                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <span class="btn btn-primary" onclick="randomer();">Générez un nouveau mot de passe ?</span>
                                    <div>
                                        <!-- has-error -->
                                        <div class="form-group">
                                            <input type="password" id="oldtxtPassword" style="margin-top: -0.3rem;" name="password" value="" required class="form-control" placeholder="Entrez l'ancien mot de passe">
                                            <button style="
                                            background: none;
                                            border: none;
                                            color: #337ab7;
                                            font-weight: 600;
                                            position: absolute;
                                            right: 1em;
                                            top: 3em;
                                            z-index: 9;
                                            " type="button" id="btnToggle3" class="toggle3"><i id="eyeIcon3" class="fa fa-eye"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                            <div class="row">
                                <button type="submit" class="btn btn-wd btn-warning" id="submitBtn" style="position: relative;
                                left: 45%;margin-top: 2.5rem;float: right;margin-right: 13rem;">
                                    <span class="btn-label">
                                        <i class="fa fa-pencil"></i>
                                    </span>
                                    <img src="{{asset('img/loading.gif')}}" class="loader" style="height: auto;
                                    width: 1.2rem;display:none;margin-left: 0.2rem;">
                                    <span class="add">Modifier</span>
                                </button>
                            </div>
                            
                            
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    function randomer(){
        document.getElementById('oldtxtPassword').value = Math.random().toString(36).slice(-8);
        // document.getElementById('randoming').style.display = "block";
    }
    let passwordInput = document.getElementById('txtPassword'),
    toggle = document.getElementById('btnToggle'),
    icon =  document.getElementById('eyeIcon');

    function togglePassword() {
    if (passwordInput.type === 'password') {
        passwordInput.type = 'text';
        icon.classList.add("fa-eye-slash");
        //toggle.innerHTML = 'hide';
    } else {
        passwordInput.type = 'password';
        icon.classList.remove("fa-eye-slash");
        //toggle.innerHTML = 'show';
    }
    }

    function checkInput() {
    //if (passwordInput.value === '') {
    //toggle.style.display = 'none';
    //toggle.innerHTML = 'show';
    //  passwordInput.type = 'password';
    //} else {
    //  toggle.style.display = 'block';
    //}
    }

    toggle.addEventListener('click', togglePassword, false);
    passwordInput.addEventListener('keyup', checkInput, false);
</script>


<script>
    let passwordInput3 = document.getElementById('oldtxtPassword'),
    toggle3 = document.getElementById('btnToggle3'),
    icon3 =  document.getElementById('eyeIcon3');

    function togglePassword() {
    if (passwordInput3.type === 'password') {
        passwordInput3.type = 'text';
        icon3.classList.add("fa-eye-slash");
        //toggle.innerHTML = 'hide';
    } else {
        passwordInput3.type = 'password';
        icon3.classList.remove("fa-eye-slash");
        //toggle.innerHTML = 'show';
    }
    }

    function checkInput() {
    //if (passwordInput.value === '') {
    //toggle.style.display = 'none';
    //toggle.innerHTML = 'show';
    //  passwordInput.type = 'password';
    //} else {
    //  toggle.style.display = 'block';
    //}
    }

    toggle3.addEventListener('click', togglePassword, false);
    passwordInput3.addEventListener('keyup', checkInput, false);
</script>

<script>
    let passwordInput1 = document.getElementById('txtPassword-confirm'),
    toggle1 = document.getElementById('btnToggle1'),
    icon1 =  document.getElementById('eyeIcon1');

    function togglePassword() {
    if (passwordInput1.type === 'password') {
        passwordInput1.type = 'text';
        icon1.classList.add("fa-eye-slash");
        //toggle.innerHTML = 'hide';
    } else {
        passwordInput1.type = 'password';
        icon1.classList.remove("fa-eye-slash");
        //toggle.innerHTML = 'show';
    }
    }

    function checkInput() {
    //if (passwordInput.value === '') {
    //toggle.style.display = 'none';
    //toggle.innerHTML = 'show';
    //  passwordInput.type = 'password';
    //} else {
    //  toggle.style.display = 'block';
    //}
    }

    toggle1.addEventListener('click', togglePassword, false);
    passwordInput1.addEventListener('keyup', checkInput, false);
</script>

<script>
    $('#updateUser').on('submit', function (e) {

                            // On empeche le navigateur de soumettre le formulaire
                            e.preventDefault();
                            $('#submitBtn').addClass("loading");
                            var $form = $(this);

                            var formdata = (window.FormData) ? new FormData($form[0]) : null;

                            var data = (formdata !== null) ? formdata : $form.serialize();
                            if($("#txtPassword-confirm").val() != $("#txtPassword").val()){
                                swal(
                                    'Erreur !',
                                    'Vos mots de passe ne sont pas identiques.',
                                    'error'
                                );
                                
                                $('#submitBtn').removeClass("loading");
                            }else if($("#txtPassword-confirm").val() == "" || $("#txtPassword").val() == "" || $("#email").val() == "" || $("#name").val() == ""){
                                swal(
                                    'Erreur !',
                                    'Vous devez remplir tous les champs.',
                                    'error'
                                );
                                $('#submitBtn').removeClass("loading");
                            }else{
                                $.ajax({
                                    url: $form.attr('action'),
                                    type: $form.attr('method'),
                                    contentType: false,
                                    processData: false,
                                    data: data,
                                    success: function (reponse) {
                                        if(reponse == "Password noc"){
                                            swal(
                                                'Erreur !',
                                                'L\'ancien mot de passe entré est incorrect',
                                                'error'
                                            );
                                            $('#submitBtn').removeClass("loading");
                                        }else{
                                            swal(
                                                'Bravo !',
                                                'Utilisateur modifié avec succès.',
                                                'success'
                                            );
                                            $('#submitBtn').removeClass("loading");
                                            window.location.href = "{{url('admin/users')}}";
                                        }
                                        
                                        
                                    },
                                    error: function (reponse) {
                                        swal(
                                            'Erreur !',
                                            'Une erreur est survenue. Veuillez reesayer plutard.',
                                            'error'
                                        );
                                        $('#submitBtn').removeClass("loading");
                                        console.log('responserror' + reponse);
                                    }

                                });
                            }

                            
                        });
</script>

@endpush



