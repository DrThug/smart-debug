@extends('Dashboard.Layout.header')
@extends('Dashboard.Layout.sidebar')

@section('content')

<style>
    .loading .fa{
        display:none !important;
    }
    .loading .add{
        display:none !important;
    }
    .loading .loader{
        display:block !important;
    }
</style>
<div class="content" style="padding-top:30px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="card ">
                    <div class="card-header ">
                        <h4 class="card-title"></h4>
                    </div>
                    <div class="card-body ">
                        <form method="POST" action="{{ route('storeUser') }}" class="form-horizontal" id="storeUser">
                        @csrf

                            
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Nom *</label>
                                    <div>
                                        <!-- has-success -->
                                        <div class="form-group">
                                            <input type="text" name="name" id="name" class="form-control" required  placeholder="Entrez le nom">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Email *</label>
                                    <div>
                                        <!-- has-error -->
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" class="form-control" required placeholder="Entrez l'email">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Rôle *</label>
                                    <div>
                                        <!-- has-success -->
                                        <div class="form-group">
                                            <select name="role" id="role" class="form-control" required>
                                                @foreach($roles as $role)

                                                    <option value="{{ $role->role }}">{{ $role->role }}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Mot de passe *</label>
                                    <div>
                                        <!-- has-error -->
                                        <div class="form-group">
                                            <input type="password" id="txtPassword" name="password" required class="form-control" placeholder="Entrez le mot de passe">
                                            <button style="
                                            background: none;
                                            border: none;
                                            color: #337ab7;
                                            font-weight: 600;
                                            position: absolute;
                                            right: 1em;
                                            top: 2.7em;
                                            z-index: 9;
                                            " type="button" id="btnToggle" class="toggle"><i id="eyeIcon" class="fa fa-eye"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Confirmation du mot de passe *</label>
                                    <div>
                                        <!-- has-error -->
                                        <div class="form-group">
                                            <input type="password" id="txtPassword-confirm" required class="form-control" placeholder="Confirmez le mot de passe">
                                            <button style="
                                            background: none;
                                            border: none;
                                            color: #337ab7;
                                            font-weight: 600;
                                            position: absolute;
                                            right: 1em;
                                            top: 2.7em;
                                            z-index: 9;
                                            " type="button" id="btnToggle1" class="toggle"><i id="eyeIcon1" class="fa fa-eye"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <button class="btn btn-wd btn-success" id="submitBtn" style="margin-top: 2.5rem;float: right;margin-right: 13rem;">
                                        <span class="btn-label">
                                            <i class="fa fa-plus"></i>
                                        </span>
                                        <img src="{{asset('img/loading.gif')}}" class="loader" style="height: auto;
                                        width: 1.2rem;display:none;margin-left: 0.2rem;">
                                        <span class="add">Ajouter</span>
                                    </button>
                                </div>
                            </div>
                            
                            
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    let passwordInput = document.getElementById('txtPassword'),
    toggle = document.getElementById('btnToggle'),
    icon =  document.getElementById('eyeIcon');

    function togglePassword() {
    if (passwordInput.type === 'password') {
        passwordInput.type = 'text';
        icon.classList.add("fa-eye-slash");
        //toggle.innerHTML = 'hide';
    } else {
        passwordInput.type = 'password';
        icon.classList.remove("fa-eye-slash");
        //toggle.innerHTML = 'show';
    }
    }

    function checkInput() {
    //if (passwordInput.value === '') {
    //toggle.style.display = 'none';
    //toggle.innerHTML = 'show';
    //  passwordInput.type = 'password';
    //} else {
    //  toggle.style.display = 'block';
    //}
    }

    toggle.addEventListener('click', togglePassword, false);
    passwordInput.addEventListener('keyup', checkInput, false);
</script>

<script>
    let passwordInput1 = document.getElementById('txtPassword-confirm'),
    toggle1 = document.getElementById('btnToggle1'),
    icon1 =  document.getElementById('eyeIcon1');

    function togglePassword() {
    if (passwordInput1.type === 'password') {
        passwordInput1.type = 'text';
        icon1.classList.add("fa-eye-slash");
        //toggle.innerHTML = 'hide';
    } else {
        passwordInput1.type = 'password';
        icon1.classList.remove("fa-eye-slash");
        //toggle.innerHTML = 'show';
    }
    }

    function checkInput() {
    //if (passwordInput.value === '') {
    //toggle.style.display = 'none';
    //toggle.innerHTML = 'show';
    //  passwordInput.type = 'password';
    //} else {
    //  toggle.style.display = 'block';
    //}
    }

    toggle1.addEventListener('click', togglePassword, false);
    passwordInput1.addEventListener('keyup', checkInput, false);
</script>

<script>
    $('#storeUser').on('submit', function (e) {

                            e.preventDefault();
                            $('#submitBtn').addClass("loading");
                            var $form = $(this);

                            var formdata = (window.FormData) ? new FormData($form[0]) : null;

                            var data = (formdata !== null) ? formdata : $form.serialize();
                            if($("#txtPassword-confirm").val() != $("#txtPassword").val()){
                                swal(
                                    'Erreur !',
                                    'Vos mots de passe ne sont pas identiques.',
                                    'error'
                                );
                                
                                $('#submitBtn').removeClass("loading");
                            }else if($("#txtPassword-confirm").val() == "" || $("#txtPassword").val() == "" || $("#email").val() == "" || $("#name").val() == ""){
                                swal(
                                    'Erreur !',
                                    'Vous devez remplir tous les champs.',
                                    'error'
                                );
                                $('#submitBtn').removeClass("loading");
                            }else{
                                $.ajax({
                                    url: $form.attr('action'),
                                    type: $form.attr('method'),
                                    contentType: false,
                                    processData: false,
                                    data: data,
                                    success: function (reponse) {
                                        if(reponse == "Email existant"){
                                            swal(
                                                'Erreur !',
                                                'Cet email existe déjà veuillez le modifier.',
                                                'error'
                                            );
                                            $('#submitBtn').removeClass("loading");
                                        }else{
                                            swal(
                                                'Bravo !',
                                                'Utilisateur ajouté avec succès.',
                                                'success'
                                            );
                                            $('#submitBtn').removeClass("loading");
                                            window.location.href = "{{url('admin/users')}}";
                                        }
                                        
                                    },
                                    error: function (reponse) {
                                        swal(
                                            'Erreur !',
                                            'Une erreur est survenue. Veuillez reesayer plutard.',
                                            'error'
                                        );
                                        $('#submitBtn').removeClass("loading");
                                        console.log('responserror' + reponse);
                                    }

                                });
                            }

                            
                        });
</script>

@endpush



