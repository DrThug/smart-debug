@extends('Dashboard.Layout.header')
@extends('Dashboard.Layout.sidebar')

@section('content')
<style>
    .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px;width: 75.2344px !important;
    height: 22px !important; }
    .toggle.ios .toggle-handle { border-radius: 20px; }
</style>
<div class="content" style="padding-top:30px;">
    <div class="container-fluid">
        <a href="{{url('admin/users/add')}}" class="btn btn-wd btn-success btn-outline">
            <span class="btn-label">
                <i class="fa fa-plus"></i>
            </span>
            Ajouter un utilisateur
        </a>
        <div class="row" style="padding-top:20px;">
            <div class="col-md-12">
                <div class="row">
        <div class="col">
          <div class="card bg-default shadow">
            <div class="card-header bg-transparent border-0">
              <h3 class="text-white mb-0">Liste des utilisateurs</h3>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-dark table-flush">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col" class="sort" data-sort="name">Noms</th>
                    <th scope="col" class="sort" data-sort="budget">Email</th>
                    <th scope="col" class="sort" data-sort="status">Rôle</th>
                    <th scope="col">Active ?</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody class="list">
                  @foreach ($utilisateur as $user)
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                        <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="{{asset('favicon.ico')}}">
                        </a>
                        <div class="media-body">
                          <span class="name mb-0 text-sm">{{ $user->name }}</span>
                        </div>
                      </div>
                    </th>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role }}</td>
                    <td><input type="checkbox" name="active" id="active" checked data-toggle="toggle" data-style="ios"></td>
                    <td class="text-right">
                        <a href="{{url('admin/users/edit')}}/{{$user->id}}"><i class="fa fa-edit"></i></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')

<script type="text/javascript">
    
</script>
@endpush



