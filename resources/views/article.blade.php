@extends('Layouts.header')
@push('css')     
@endpush

@section('contentFront')

    <div id="overviews" class="section wb" style="background:#2f2c3d;font:700 1.375rem/1.75rem 'Montserrat', sans-serif !important;">
        <div class="container">
            <div class="section-title row text-center">
                <div class="col-md-8 offset-md-2">
                    <h3>LISTE DES ARTICLES</h3>
                </div>
            </div><!-- end title -->

            <hr class="invis"> 

            <div class="row"> 
                @foreach ($articles as $bug)
                    
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="blog-item" style="background: #262431;">
                            <div class="image-blog">
                            <img src="{{asset('img/blog-1.jpg')}}" alt="" class="img-fluid">
                            </div>
                            <div class="meta-info-blog">
                                <span><i class="fa fa-calendar"></i> <a href="#" style="color:#00c9db;">{{ $bug->created_at }}</a> </span>
                            <span><i class="fa fa-tag"></i>  <a href="#" style="color:#00c9db;">{{ $bug->intitule }}</a> </span>
                                <span><i class="fa fa-comments"></i> <a href="#" style="color:#00c9db;">12 Commentaires</a></span>
                            </div>
                            <div class="blog-title">
                                <h2><a href="{{url('/blog/')}}/{{$bug->id}}" title="" style="color:white;">{{ $bug->titre }}</a></h2>
                            </div>
                            <div class="blog-desc">
                                <p>{!! $bug->contenu !!}</p>
                            </div><br>
                            <div class="blog-button">
                                <a class="hover-btn-new orange" href="{{url('/blog/')}}/{{$bug->id}}"><span>Lire plus<span></a>
                            </div>
                        </div>
                    </div><!-- end col -->

                @endforeach
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end section -->

@endsection