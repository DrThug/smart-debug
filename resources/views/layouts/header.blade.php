<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="Free mobile app HTML landing page template to help you build a great online presence for your app which will convert visitors into users">
    <meta name="author" content="Inovatik">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
	<meta property="og:site_name" content="" /> <!-- website name -->
	<meta property="og:site" content="" /> <!-- website link -->
	<meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
	<meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
	<meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
	<meta property="og:url" content="" /> <!-- where do you want your post to link to -->
	<meta property="og:type" content="article" />

    <!-- Website Title -->
    <title>SMART-DEBUG</title>
    <link href="{{asset('css/blog-style/style.css')}}" rel="stylesheet">  
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet">
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('css/fontawesome-all.css')}}" rel="stylesheet">
    <link href="{{asset('css/swiper.css')}}" rel="stylesheet">
	<link href="{{asset('css/magnific-popup.css')}}" rel="stylesheet">
	<link href="{{asset('css/popupLogin.css')}}" rel="stylesheet">
	<link href="{{asset('css/styles.css')}}" rel="stylesheet">
	
	<!-- Favicon  -->
    <link rel="icon" href="{{asset('favicon.ico')}}">
    @stack('css')
</head>
<body data-spy="scroll" data-target=".fixed-top">
    
    <!-- Preloader -->
	<div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- end of preloader -->
    <style>
        .activee{
            color: #00c9db !important;
        }
    </style>
    

    <!-- Navbar -->
    <nav class="navbar navbar-expand-md navbar-dark navbar-custom fixed-top">
        <!-- Text Logo - Use this if you don't have a graphic logo -->
        <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Leno</a> -->

        <!-- Image Logo -->
        <a class="navbar-brand logo-image" href="" style="text-decoration: none;
    font-size: 30px;">SMART-DEBUG</a> 
        
        <!-- Mobile Menu Toggle Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-awesome fas fa-bars"></span>
            <span class="navbar-toggler-awesome fas fa-times"></span>
        </button>
        <!-- end of mobile menu toggle button -->

        <div class="collapse navbar-collapse" id="navbarsExampleDefault" style="position: relative;
    right: 3rem;">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    @if($section == "")
                        <a class="nav-link page-scroll activee" href="/">ACCUEIL <span class="sr-only">(current)</span></a>
                    @else
                    <a class="nav-link page-scroll" href="/">ACCUEIL <span class="sr-only">(current)</span></a>
                    @endif
                </li>
                <li class="nav-item">
                    @if($section == "Bugs")
                        <a class="nav-link page-scroll activee" href="{{url('bug')}}">BUGS <span class="sr-only">(current)</span></a>
                    @else
                        <a class="nav-link page-scroll" href="{{url('bug')}}">BUGS <span class="sr-only">(current)</span></a>
                    @endif
                </li>
                <li class="nav-item">
                    @if($section == "Article")
                        <a class="nav-link page-scroll activee" href="{{url('blog')}}">BLOG <span class="sr-only">(current)</a>
                    @else
                        <a class="nav-link page-scroll" href="{{url('blog')}}">BLOG <span class="sr-only">(current)</a>
                    @endif
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#contact">CONTACT</a>
                </li>
            </ul>
            <span class="fa-stack">
                @if (!empty(Auth::user()->id))
                    <a href="{{url('admin')}}" style="display: inline-block;
                    padding: 1rem 1rem 1rem 1rem;
                    border: 0.125rem solid #00c9db;
                    position: relative;
                    bottom: 0.5rem;
                    border-radius: 2rem;
                    background-color: #00c9db;
                    color: #fff;
                    font: 700 0.75rem/0 'Montserrat', sans-serif;
                    text-decoration: none;
            transition: all 0.2s;">Dashboard</a>
                @else
                    <a class="cd-signin" id="loginPop" href="#" style="display: inline-block;
                    padding: 1rem 1rem 1rem 1rem;
                    border: 0.125rem solid #00c9db;
                    position: relative;
                    bottom: 0.5rem;
                    border-radius: 2rem;
                    background-color: #00c9db;
                    color: #fff;
                    font: 700 0.75rem/0 'Montserrat', sans-serif;
                    text-decoration: none;
                    transition: all 0.2s;"> LOGIN/REGISTER</a>
                @endif
                
            </span>
        </div>
    </nav> <!-- end of navbar -->
    <!-- end of navbar -->
    <div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
		<div class="cd-user-modal-container"> <!-- this is the container wrapper -->
			<ul class="cd-switcher">
				<li><a href="#0">Sign in</a></li>
				<li><a href="#0">New account</a></li>
			</ul>
            <style>
                .loading .fa{
                    display:none !important;
                }
                .loading .add{
                    display:none !important;
                }
                .loading .loader{
                    display:block !important;
                }
            </style>

			<div id="loginForm"> <!-- log in form -->
				<form class="cd-form">
                    @csrf
					<p class="fieldset">
						<label class="image-replace cd-email" for="signin-email">E-mail</label>
						<input class="full-width has-padding has-border" id="email" name="email" type="email" placeholder="E-mail">
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<label class="image-replace cd-password" for="signin-password">Password</label>
						<input class="full-width has-padding has-border"  id="password" type="password" name="password"  placeholder="Password">
						<a href="#0" class="hide-password">Hide</a>
						<span class="cd-error-message">Identifiants incorrects</span>
					</p>

					<p class="fieldset">
						<input type="checkbox" name="remember" id="remember" checked>
						<label for="remember-me" style="color: gray;">Remember me</label>
					</p>

					<p class="fieldset">
                        <button type="submit" class="btn btn-large btn-success" id="submitBtn" style="margin-top: 2.5rem;width:100%;">
                            <img src="{{asset('img/loading.gif')}}" class="loader" style="height: auto;
                            width: 1.5rem;display:none;margin-left: 49%;">
                            <span class="add">Se Connecter</span>
                        </button>
					</p>
				</form>
				
				<p class="cd-form-bottom-message"><a href="#0">Forgot your password?</a></p>
				<!-- <a href="#0" class="cd-close-form">Close</a> -->
			</div> <!-- cd-login -->

			<div id="cd-signup"> <!-- sign up form -->
				<form class="cd-form" id="registerForm">
                    @csrf
					<p class="fieldset">
						<label class="image-replace cd-username" for="name">Nom</label>
						<input class="full-width has-padding has-border"  id="name" name="name" type="name" placeholder="Nom">
						<span class="cd-error-message">Error message here!</span>
					</p>
					<p class="fieldset">
						<label class="image-replace cd-username" for="signup-username">Email</label>
						<input class="full-width has-padding has-border"  id="emailer" name="email" type="email" placeholder="Email">
						<span class="cd-error-message">Error message here!</span>
					</p>
                    <p class="fieldset">
						<label class="image-replace cd-password" for="signup-password">Mot de passe</label>
						<input class="full-width has-padding has-border" id="signup-password" type="text"  placeholder="Password">
						<a href="#0" class="hide-password">Hide</a>
						<span class="cd-error-message">Error message here!</span>
					</p>
                    <p class="fieldset">
						<label class="image-replace cd-password" for="signup-password1">Confirmez votre mot de passe</label>
						<input class="full-width has-padding has-border" id="signup-password1" type="text"  placeholder="Password confirm">
						<a href="#0" class="hide-password">Hide</a>
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
                        <button type="submit" class="btn btn-large btn-success" id="submitBtn1" style="margin-top: 2.5rem;width:100%;">
                            <img src="{{asset('img/loading.gif')}}" class="loader" style="height: auto;
                            width: 1.5rem;display:none;margin-left: 49%;">
                            <span class="add">S'enregistrer</span>
                        </button>
					</p>
				</form>

				<!-- <a href="#0" class="cd-close-form">Close</a> -->
			</div> <!-- cd-signup -->

			<div id="cd-reset-password"> <!-- reset password form -->
				<p class="cd-form-message" style="color: gray;">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>

				<form class="cd-form">
					<p class="fieldset">
						<label class="image-replace cd-email" for="reset-email">E-mail</label>
						<input class="full-width has-padding has-border" id="reset-email" type="email" placeholder="E-mail">
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<input class="full-width has-padding" type="submit" value="Reset password">
					</p>
				</form>

				<p class="cd-form-bottom-message"><a href="#0">Back to log-in</a></p>
			</div> <!-- cd-reset-password -->
			<a href="#0" class="cd-close-form">Close</a>
		</div> <!-- cd-user-modal-container -->
	</div> <!-- cd-user-modal -->

    <!-- Header -->
    <header id="header" class="header">
        @if(!$section || empty($section))
            <div class="header-content">
                <div class="text-container" style="margin: 0;margin-left: 2.5rem;">
                    <h1 text-align="center" style="text-align:center;">SOLUTION DE DEBOGAGE <br> POUR <span id="js-rotating">DEVELOPPEURS, CODEURS, SMARTCODEGROUP</span></h1>
                </div>
            </div> <!-- end of header-content -->
        @elseif ($section == "Bugs")
            <div class="header-content" style="padding-top:7rem;padding-bottom:0rem;">
                <div class="text-container" style="margin: 0;margin-left: 2.5rem;">
                    {{-- <h1 text-align="center" style="text-align:center;">SOLUTION DE DEBOGAGE <br> POUR <span id="js-rotating">DEVELOPPEURS, CODEURS, SMARTCODEGROUP</span></h1> --}}
                </div>
            </div>
        @elseif ($section == "Article")
            <div class="header-content" style="padding-top:7rem;padding-bottom:0rem;">
                <div class="text-container" style="margin: 0;margin-left: 2.5rem;">
                    {{-- <h1 text-align="center" style="text-align:center;">SOLUTION DE DEBOGAGE <br> POUR <span id="js-rotating">DEVELOPPEURS, CODEURS, SMARTCODEGROUP</span></h1> --}}
                </div>
            </div>
        @endif
        
    </header> <!-- end of header -->
    <!-- end of header -->

    @yield('contentFront')

    <!-- Footer -->
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="footer-col">
                    <h4>A propos de SMARTCODEGROUP</h4>
                    <p>Nous sommes une équipe de jeune développeur lorem impsum text lorem ipsum</p>
                </div>
            </div> <!-- end of col -->
            <div class="col-md-4">
                <div class="footer-col middle">
                    <h4>Site web</h4>
                    <ul class="list-unstyled li-space-lg">
                        <li class="media">
                            <i class="fas fa-square"></i>
                            <div class="media-body"><a class="turquoise" href="https://smartcodegroup.com">https://smartcodegroup.com</a></div>
                        </li>
                    </ul>
                </div>
            </div> <!-- end of col -->
            <div class="col-md-4">
                <div class="footer-col last">
                    <h4 style="text-transform:uppercase;">réseaux sociaux</h4>
                    <span class="fa-stack">
                        <a href="#your-link">
                            <i class="fas fa-circle fa-stack-2x"></i>
                            <i class="fab fa-facebook-f fa-stack-1x"></i>
                        </a>
                    </span>
                    <span class="fa-stack">
                        <a href="#your-link">
                            <i class="fas fa-circle fa-stack-2x"></i>
                            <i class="fab fa-twitter fa-stack-1x"></i>
                        </a>
                    </span>
                    <span class="fa-stack">
                        <a href="#your-link">
                            <i class="fas fa-circle fa-stack-2x"></i>
                            <i class="fab fa-google-plus-g fa-stack-1x"></i>
                        </a>
                    </span>
                    <span class="fa-stack">
                        <a href="#your-link">
                            <i class="fas fa-circle fa-stack-2x"></i>
                            <i class="fab fa-instagram fa-stack-1x"></i>
                        </a>
                    </span>
                    <span class="fa-stack">
                        <a href="#your-link">
                            <i class="fas fa-circle fa-stack-2x"></i>
                            <i class="fab fa-linkedin-in fa-stack-1x"></i>
                        </a>
                    </span>
                </div> 
            </div> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div> <!-- end of footer -->  
<!-- end of footer -->


<!-- Copyright -->
<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <p class="p-small">Copyright © 2020 - SMARTCODEGROUP</a></p>
            </div> <!-- end of col -->
        </div> <!-- enf of row -->
    </div> <!-- end of container -->
</div> <!-- end of copyright --> 
<!-- end of copyright -->

    
<!-- Scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
{{-- <script src="{{asset('js/jquery.min.js')}}"></script> <!-- jQuery for Bootstrap's JavaScript plugins --> --}}
<script src="{{asset('js/popper.min.js')}}"></script> <!-- Popper tooltip library for Bootstrap -->
<script src="{{asset('js/bootstrap.min.js')}}"></script> <!-- Bootstrap framework -->
<script src="{{asset('js/jquery.easing.min.js')}}"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
<script src="{{asset('js/swiper.min.js')}}"></script> <!-- Swiper for image and text sliders -->
<script src="{{asset('js/jquery.magnific-popup.js')}}"></script> <!-- Magnific Popup for lightboxes -->
<script src="{{asset('js/morphext.min.js')}}"></script> <!-- Morphtext rotating text in the header -->
<script src="{{asset('js/validator.min.js')}}"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
<script src="{{asset('js/scripts.js')}}"></script> <!-- Custom scripts -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    jQuery(document).ready(function($){
        var $form_modal = $('.cd-user-modal'),
            $form_login = $form_modal.find('#loginForm'),
            $form_signup = $form_modal.find('#cd-signup'),
            $form_forgot_password = $form_modal.find('#cd-reset-password'),
            $form_modal_tab = $('.cd-switcher'),
            $tab_login = $form_modal_tab.children('li').eq(0).children('a'),
            $tab_signup = $form_modal_tab.children('li').eq(1).children('a'),
            $forgot_password_link = $form_login.find('.cd-form-bottom-message a'),
            $back_to_login_link = $form_forgot_password.find('.cd-form-bottom-message a'),
            $main_nav = $('#loginPop');

        //open modal
        $main_nav.on('click', function(event){

            $(".collapse.navbar-collapse").children('ul').removeClass('is-visible');
                //show modal layer
                $form_modal.addClass('is-visible');	
                //show the selected form
                ( $(event.target).is('.cd-signup') ) ? signup_selected() : login_selected();

        });

        //close modal
        $('.cd-user-modal').on('click', function(event){
            if( $(event.target).is($form_modal) || $(event.target).is('.cd-close-form') ) {
                $form_modal.removeClass('is-visible');
            }	
        });
        //close modal when clicking the esc keyboard button
        $(document).keyup(function(event){
            if(event.which=='27'){
                $form_modal.removeClass('is-visible');
            }
        });

        //switch from a tab to another
        $form_modal_tab.on('click', function(event) {
            event.preventDefault();
            ( $(event.target).is( $tab_login ) ) ? login_selected() : signup_selected();
        });

        //hide or show password
        $('.hide-password').on('click', function(){
            var $this= $(this),
                $password_field = $this.prev('input');
            
            ( 'password' == $password_field.attr('type') ) ? $password_field.attr('type', 'text') : $password_field.attr('type', 'password');
            ( 'Hide' == $this.text() ) ? $this.text('Show') : $this.text('Hide');
            //focus and move cursor to the end of input field
            $password_field.putCursorAtEnd();
        });

        //show forgot-password form 
        $forgot_password_link.on('click', function(event){
            event.preventDefault();
            forgot_password_selected();
        });

        //back to login from the forgot-password form
        $back_to_login_link.on('click', function(event){
            event.preventDefault();
            login_selected();
        });

        function login_selected(){
            $form_login.addClass('is-selected');
            $form_login.css('display','block');
            $form_signup.removeClass('is-selected');
            $form_forgot_password.removeClass('is-selected');
            $tab_login.addClass('selected');
            $tab_signup.removeClass('selected');
        }

        function signup_selected(){
            $form_login.removeClass('is-selected');
            $form_login.css('display','none');
            $form_signup.addClass('is-selected');
            $form_forgot_password.removeClass('is-selected');
            $tab_login.removeClass('selected');
            $tab_signup.addClass('selected');
        }

        function forgot_password_selected(){
            $form_login.removeClass('is-selected');
            $form_login.css('display','none');
            $form_signup.removeClass('is-selected');
            $form_forgot_password.addClass('is-selected');
        }

        //REMOVE THIS - it's just to show error messages 
        $form_login.find('#submitBtn').on('click', function(event){
            event.preventDefault();
            
            $('#submitBtn').addClass("loading");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('#loginForm input[name="_token"]').val()
                }
            });
            var email = $("#loginForm input[name='email']").val();
            var password = $("#loginForm input[name='password']").val();

            $.ajax({
                url: '{{ route('login') }}',
                type: 'POST',
                data: {
                    email: email,
                    password: password
                },
                success: function(data) {
                        console.log(data);
                    if (data.success == "Login ok.") {
                        $('#submitBtn').removeClass("loading");
                        window.location.href = "/admin";
                    }else if (data.error == "Identifiants invalides.") {
                        $('#submitBtn').removeClass("loading");
                                        swal(
                                            'Erreur !',
                                            'Identifiants invalides.',
                                            'error'
                                        );

                    }else {
                        $('#submitBtn').removeClass("loading");
                                        swal(
                                            'Erreur !',
                                            'Une erreur est survenue. Veuillez reesayer plutard.',
                                            'error'
                                        );
                    }
                }
            });

        });

        

        //REMOVE THIS - it's just to show error messages 
        $form_signup.find('#submitBtn1').on('click', function(event){
            event.preventDefault();
            
            $('#submitBtn1').addClass("loading");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('#registerForm input[name="_token"]').val()
                }
            });
            var name = $("#registerForm input[name='name']").val();
            var email = $("#registerForm input[name='email']").val();
            var password = $("#registerForm #signup-password").val();
            var password1 = $("#registerForm #signup-password1").val();

            if (password == password1) {
                $.ajax({
                url: '{{ route('register') }}',
                type: 'POST',
                data: {
                    name: name,
                    email: email,
                    password: password,
                    password_confirmation: password
                },
                success: function(data) {
                        console.log(data);
                        if (data.success == "Inscription ok.") {
                            $('#submitBtn1').removeClass("loading");
                            window.location.href = "/admin";
                        }else {
                            $('#submitBtn1').removeClass("loading");
                                            swal(
                                                'Erreur !',
                                                'Une erreur est survenue. Veuillez reesayer plutard.',
                                                'error'
                                            );
                        }
                    }
                });
            }else{
                $('#submitBtn1').removeClass("loading");
                swal(
                                                'Erreur !',
                                                'Vos mots de passe ne correspondent pas.',
                                                'error'
                                            );
            }

            

        });


        //IE9 placeholder fallback
        //credits http://www.hagenburger.net/BLOG/HTML5-Input-Placeholder-Fix-With-jQuery.html
        // if(!Modernizr.input.placeholder){
        //     $('[placeholder]').focus(function() {
        //         var input = $(this);
        //         if (input.val() == input.attr('placeholder')) {
        //             input.val('');
        //         }
        //     }).blur(function() {
        //         var input = $(this);
        //         if (input.val() == '' || input.val() == input.attr('placeholder')) {
        //             input.val(input.attr('placeholder'));
        //         }
        //     }).blur();
        //     $('[placeholder]').parents('form').submit(function() {
        //         $(this).find('[placeholder]').each(function() {
        //             var input = $(this);
        //             if (input.val() == input.attr('placeholder')) {
        //                 input.val('');
        //             }
        //         })
        //     });
        // }

    });


    //credits https://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
    jQuery.fn.putCursorAtEnd = function() {
        return this.each(function() {
            // If this function exists...
            if (this.setSelectionRange) {
                // ... then use it (Doesn't work in IE)
                // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
                var len = $(this).val().length * 2;
                this.setSelectionRange(len, len);
            } else {
                // ... otherwise replace the contents with itself
                // (Doesn't work in Google Chrome)
                $(this).val($(this).val());
            }
        });
    };

    jQuery('#cody-info ul li').eq(1).on('click', function(){
    $('#cody-info').hide();
    });
</script>
@stack('scripts')
</body>
</html>