<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBugsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bugs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('titre');
            $table->text('solution');
            $table->bigInteger('categorie')->unsigned();
            $table->bigInteger('idUser')->unsigned();
            $table->foreign('idUser')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
            $table->foreign('categorie')->references('id')->on('categories')
						->onDelete('cascade')
						->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bugs');
    }
}
