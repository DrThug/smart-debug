<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('roles')->insert([
            'role' => 'Super administrateur'
        ]);
        DB::table('roles')->insert([
            'role' => 'Employe'
        ]);

        DB::table('users')->insert([
            'name' => 'SmartCode',
            'email' => 'contact@smartcode.com',
            'role' => 'Super administrateur',
            'active' => '1',
            'password' => Hash::make('12345678')
        ]);

    }
}
