<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bugs extends Model
{
    protected $fillable = [
        'titre', 'solution', 'categorie','idUser',
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function categories()
    {
        return $this->belongsTo(categories::class);
    }
}
