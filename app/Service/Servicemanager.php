<?php

namespace App\Service;

use App\User;
use App\Role;
use App\articles;
use App\bugs;
use App\categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ServiceManager
{
    /* ---------------------- CREATE --------------------- */

    public function CreateUser($request)
    {
        $users = User::all()->where('active', '1');
        $i = 1;
        foreach ($users as $user) {
            if ($request['email'] == $user->email) {
                $i = 0;
                break;
            }
        }
        
        if ($i == 1) {
            return User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'role' => $request['role'],
                'active' => '1',
                'password' => Hash::make($request['password'])
            ]);
        }elseif($i == 0){
            return "Email existant";
        }

    }

    public function sendSlackNotification($text)
    {
        $headers = array();
        $headers[] = 'Content-type: application/json';
        $data = json_encode(["text" => $text]);
        
        $webhookUrl = "https://hooks.slack.com/services/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";
    
        $ch = curl_init($webhookUrl);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_exec($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
    
        return $statusCode == 200 ? true : false;
    }


    public function CreateArticle($request)
    {
        if ($request['element'] == "bugs") {
            $text = "
            :warning: *Un nouveau bug résolu :heavy_check_mark: enregistré sur le site* :warning: 
            
            *Titre du bug*: ".$request['titreBug']."
            *Solution (Cliquez sur le lien)*: http://corona.smartdocteur.com
            *Catégorie*: PHP
            *Publié par*: Contact Smartcode";
            $headers = array();
            $headers[] = 'Content-type: application/json';
            $data = json_encode(["text" => $text]);
            
            $webhookUrl = "https://hooks.slack.com/services/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";
        
            $ch = curl_init($webhookUrl);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_exec($ch);
            $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
    
            return $statusCode != 200 ? false : bugs::create([
                'titre' => $request['titreBug'],
                'solution' => $request['solution'],
                'categorie' => $request['categorie'],
                'idUser' => $request['idUser']
            ]);
            
        }elseif($request['element'] == "article"){
            return articles::create([
                'titre' => $request['titreArticle'],
                'contenu' => $request['contenu'],
                'categorie' => $request['categorie'],
                'idUser' => $request['idUser']
            ]);
        }

    }

    /* ---------------------- READ ---------------------- */

    public function viewAllUsers()
    {
        $users = User::all()->where('active', '1');
        return $users;
    }

    public function viewAllArticles()
    {
        $articles = DB::table("articles")
        ->join("categories","articles.categorie","=","categories.id")
        ->join("users","articles.idUser","=","users.id")
        ->select('articles.*', 'categories.intitule', 'users.name')
        ->get();
        return $articles;
    }
    public function viewAllBugs()
    {
        $bugs = DB::table("bugs")
        ->join("categories","bugs.categorie","=","categories.id")
        ->join("users","bugs.idUser","=","users.id")
        ->select('bugs.*', 'categories.intitule', 'users.name')
        ->orderBy('id', 'desc')
        ->get();
        return $bugs;
    }

    public function viewAllBugById($id)
    {
        $bugs = DB::table("bugs")
        ->join("categories","bugs.categorie","=","categories.id")
        ->join("users","bugs.idUser","=","users.id")
        ->select('bugs.*', 'categories.intitule', 'users.name')
        ->where('bugs.id',$id)
        ->orderBy('id', 'desc')
        ->get();
        return $bugs;
    }

    public function viewAllArticleById($id)
    {
        $bugs = DB::table("articles")
        ->join("categories","articles.categorie","=","categories.id")
        ->join("users","articles.idUser","=","users.id")
        ->select('articles.*', 'categories.intitule', 'users.name')
        ->where('articles.id',$id)
        ->orderBy('id', 'desc')
        ->get();
        return $bugs;
    }

    public function viewLastArticles()
    {
        $articles = DB::table("articles")
        ->join("categories","articles.categorie","=","categories.id")
        ->join("users","articles.idUser","=","users.id")
        ->select('articles.*', 'categories.intitule', 'users.name')
        ->orderBy('id', 'desc')
        ->limit(1)
        ->get();
        return $articles;
    }
    public function viewLastBugs()
    {
        $bugs = DB::table("bugs")
        ->join("categories","bugs.categorie","=","categories.id")
        ->join("users","bugs.idUser","=","users.id")
        ->select('bugs.*', 'categories.intitule', 'users.name')
        ->get();
        return $bugs;
    }
    
    public function viewAllRoles()
    {
        $users = Role::all();
        return $users;
    }
    
    public function viewAllCategories()
    {
        $users = categories::all();
        return $users;
    }
    
    public function getUserId($id)
    {
        $user = User::find($id);
        return $user;
    }


    /* ---------------------- UPDATE --------------------- */

    public function setUser($request)
    {
        return DB::table('users')->where([
                ['id', $request['id']]
            ])->update([
                'name' => $request['name'],
                'email' => $request['email'],
                'role' => $request['role'],
                'active' => '1',
                'password' => Hash::make($request['password'])
        ]);
        
    }

    //$2y$10$5GSNNbx2SMkaq7Mfv.yVHeU0NWWkydBQXmwJFaIOzI.FAbEwMQ4hS


    /* ---------------------- DELETE --------------------- */


    public function deleteUserId($user_id)
    {
        $user = DB::table('users')
            ->where('id', $user_id)
            ->delete();
        return $user;
    }



}
