<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class plustard extends Model
{
    protected $guard = [];
    
    public function article()
    {
        return $this->belongsTo(articles::class);
    }
    
    public function bugs()
    {
        return $this->belongsTo(bugs::class);
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
