<?php

namespace App\Http\Controllers;

use App\Service\ServiceManager;
use Illuminate\Http\Request;

class BugController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $service;

    public function __construct(ServiceManager $service)
    {
        // $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $bugs = $this->service->viewAllBugs();
        $section = "Bugs";
        return view('bug',compact('section','bugs'));
    }

    public function detail($id)
    {
        $bugs = $this->service->viewAllBugById($id);
        $section = "Bugs";
        return view('bug-detail',compact('bugs','section'));
    }
}
