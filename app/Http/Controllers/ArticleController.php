<?php

namespace App\Http\Controllers;

use App\Service\ServiceManager;
use App\User;
use App\bugs;
use App\articles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $service;

    public function __construct(ServiceManager $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $articles = $this->service->viewAllArticles();
        $bugs = $this->service->viewAllBugs();
        $section = "Articles/Bugs";
        return view('dashboard.articles.index',compact('articles','section','bugs'));
    }

    
    public function add()
    {
        $categories = $this->service->viewAllCategories();
        $section = "Ajouter";
        return view('dashboard.articles.store',compact('categories','section'));
    }

    
    public function store(Request $request)
    {
        $storeArticle = $this->service->CreateArticle($request);
        return $storeArticle;
    }

    public function edit($id)
    {
        $utilisateur = $this->service->getUserId($id);
        $roles = $this->service->viewAllRoles();
        $section = "Modifier";
        return view('dashboard.articles.edit',compact('utilisateur','roles','section'));
    }

    public function update(Request $request)
    {
        $utilisateur = $this->service->setUser($request);
        return $utilisateur;
    }
}
