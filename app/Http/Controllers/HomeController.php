<?php

namespace App\Http\Controllers;

use App\Service\ServiceManager;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ServiceManager $service)
    {
        $this->service = $service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $articles = $this->service->viewLastArticles();
        $bugs = $this->service->viewLastBugs();
        $section = "";
        return view('welcome',compact('articles','bugs','section'));
    }
}
