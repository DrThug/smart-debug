<?php

namespace App\Http\Controllers;

use App\Service\ServiceManager;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $service;

    public function __construct(ServiceManager $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $utilisateur = $this->service->viewAllUsers();
        $section = "Utilisateurs";
        return view('dashboard.users.index',compact('utilisateur','section'));
    }

    
    public function add()
    {
        $roles = $this->service->viewAllRoles();
        $section = "Ajouter";
        return view('dashboard.users.store',compact('roles','section'));
    }

    
    public function store(Request $request)
    {
        $storeUser = $this->service->CreateUser($request);
        return $storeUser;
    }

    public function edit($id)
    {
        $utilisateur = $this->service->getUserId($id);
        $roles = $this->service->viewAllRoles();
        $section = "Modifier";
        return view('dashboard.users.edit',compact('utilisateur','roles','section'));
    }

    public function update(Request $request)
    {
        $utilisateur = $this->service->setUser($request);
        return $utilisateur;
    }
}
