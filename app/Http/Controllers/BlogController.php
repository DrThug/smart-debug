<?php

namespace App\Http\Controllers;

use App\Service\ServiceManager;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $service;

    public function __construct(ServiceManager $service)
    {
        // $this->middleware('auth');
        $this->service = $service;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $articles = $this->service->viewAllArticles();
        $section = "Article";
        return view('article',compact('section','articles'));
    }

    public function detail($id)
    {
        $articles = $this->service->viewAllArticleById($id);
        $section = "Article";
        return view('article-detail',compact('articles','section'));
    }
}
