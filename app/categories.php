<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categories extends Model
{
    protected $guard = [];

	public function articles()
	{
		return $this->hasMany(articles::class);
    }
    
	public function bugs()
	{
		return $this->hasMany(bugs::class);
	}
}
