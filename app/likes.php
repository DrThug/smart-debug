<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class likes extends Model
{
    protected $guard = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function article()
    {
        return $this->belongsTo(articles::class);
    }
    
    public function bugs()
    {
        return $this->belongsTo(bugs::class);
    }
}
