<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class newsletter extends Model
{
    protected $guard = [];
    
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
